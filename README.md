# Touchcard Fonts

These fonts are used for Chrome Headless rendering.

They're taken from the Google Fonts repo: https://github.com/google/fonts

We're only pulling in the fonts we actually need.

